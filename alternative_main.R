# Du kannst natürlich alles auch in einem skript schreiben.
# Kommentare sorgen für Ordnung

## warm-up
print("schwänz")

## installiere packages, die wir brauchen
# install.packages("data.table")
# install.packages("ggplot2")
# install.packages("tidyverse")
# install.packages("magrittr")

## lade die packages
library(data.table)
library(tidyverse)
library(ggplot2)
library(magrittr)

## gibt den Arbeitsordner aus
print(getwd())

## definiert den Arbeitsordner nach unserem willen
ordner_name <- "/Users/Thomsn/Desktop/island_in_the_sun/rrrrrrr/2021_07_h1" # musst du ändern!
setwd(ordner_name) # setzt den arbeitsordner fest.



##### HAUSAUFGABE #####
## wir laden die Daten in den Arbeitsspeicher
data <- "data/ADH_Kinetik_selection.csv"
(dataframe <- fread(data)) # Klammersetzung gibt (1) fread(data) in die Ausgabe und (2) speichert es als variabel dataframe

## wir definieren 1/v neu (mittels pipe operator)
dataframe %>% .[, v_inverse:=1/`v [µmol/min]`]
print(dataframe)

## wir plotten 1/[S] gegen 1/v
dataframe %>% 
ggplot(., aes(`1/[S]`, v_inverse)) +
  geom_point()

## dann machen wir ein lineares modell der inversen
inv_lin_mod <- lm(v_inverse~`1/[S]`, dataframe) # lm ist der Befehl für ein lineares modell. hier ist v_inverse die response von 1/[S]
print(inv_lin_mod)
dataframe %>% 
  ggplot(., aes(`1/[S]`, v_inverse)) +
  geom_point() +
  geom_abline(intercept = inv_lin_mod$coefficients[1], slope = inv_lin_mod$coefficients[2], col="grey")

## jetzt brauchen wir eine Funktion die aus dem lm der inversen ein hyperbolisches modell der nicht-inversen macht:
## dafür definieren wir eine funktion von S inpt werten und einem gegebenen lm
hyperbolic_curve <- function(s_value, lin_model) {
  k = lin_model$coefficients[2] / lin_model$coefficients[1]
  m = 1 / lin_model$coefficients[1]
  v = m*s_value / (k+s_value)
  v
}

## diese verwenden wir, um die hyperbolische Kurve abzuschätzen
df <- data.table(S=seq(0,50, length.out=1000))
df[, v:=hyperbolic_curve(S, inv_lin_mod)] # Abschätzung der hyperbolischen regression
dataframe %>% 
  ggplot(., aes(`cEtOH [mM]`, `v [µmol/min]`)) +
  geom_point() +
  geom_line(data=df, aes(S, v), col="blue") +
  labs(y = "reaction velocity [µmol/min]", x = "substrate concentration [mM]")

## diese Abbildung speichern wir noch
output_name = "data/hyperbolic_regression_alternative.pdf"
ggsave(output_name, device = "pdf", width = 9, height = 6) # speichert das pdf

dev.off() # schließt den plotting device, spart arbeitspeicher

# ENDE